<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

Auth::routes();

Route::group([ 'middleware' => ['auth'] ],
    function(){

        //Clientes
        Route::get('clientes', [App\Http\Controllers\ClientesController::class, 'index'])->name('clientes');
        Route::get('clientes/getJson', [App\Http\Controllers\ClientesController::class, 'getJson'])->name('clientes.getJson');
        Route::get('/clientes/create' , [App\Http\Controllers\ClientesController::class, 'create'] )->name('clientes.create');
        Route::post('clientes' , [App\Http\Controllers\ClientesController::class, 'store'] )->name('clientes.store');
        Route::get('clientes/{cliente}' , [App\Http\Controllers\ClientesController::class, 'edit'] )->name('clientes.edit');
        Route::put('clientes/{cliente}' , [App\Http\Controllers\ClientesController::class, 'update'] )->name('clientes.update');
        Route::delete('clientes/destroy/{cliente}' , [App\Http\Controllers\ClientesController::class, 'destroy'] )->name('clientes.destroy');

        //Productos
        Route::get('productos', [App\Http\Controllers\ProductosController::class, 'index'])->name('productos');
        Route::get('productos/getJson', [App\Http\Controllers\ProductosController::class, 'getJson'])->name('productos.getJson');
        Route::get('/productos/create' , [App\Http\Controllers\ProductosController::class, 'create'] )->name('productos.create');
        Route::post('productos' , [App\Http\Controllers\ProductosController::class, 'store'] )->name('productos.store');
        Route::get('productos/{producto}' , [App\Http\Controllers\ProductosController::class, 'edit'] )->name('productos.edit');
        Route::put('productos/{producto}' , [App\Http\Controllers\ProductosController::class, 'update'] )->name('productos.update');
        Route::delete('productos/destroy/{producto}', [App\Http\Controllers\ProductosController::class, 'destroy'])->name('productos.destroy');
        Route::get('productos/buscarPrecio/{producto}', [App\Http\Controllers\ProductosController::class, 'buscarPrecio'])->name('productos.buscarPrecio');

        //Ventas
        Route::get('ventas', [App\Http\Controllers\VentasController::class, 'index'])->name('ventas');
        Route::get('ventas/getJson', [App\Http\Controllers\VentasController::class, 'getJson'])->name('ventas.getJson');
        Route::get('/ventas/create' , [App\Http\Controllers\VentasController::class, 'create'] )->name('ventas.create');
        Route::post('ventas' , [App\Http\Controllers\VentasController::class, 'store'] )->name('ventas.store');
        Route::get('ventas/show/{venta}' , [App\Http\Controllers\VentasController::class, 'show'] )->name('ventas.show');

    });



