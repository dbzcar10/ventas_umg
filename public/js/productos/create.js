var validator = $("#ProductoForm").validate({
    ignore: [],
    onkeyup: false,
    onclick: false,
    //onfocusout: false,
    rules: {
        nombre: {
            required: true
        },
        precio: {
            required: true,
        },
    },
    messages: {
        nombre: {
            required: "Por favor, ingrese nombre"
        },
        precio: {
            required: "Por favor, ingrese precio",
        },
    }
});
