    $(function ()
    {
        var indice = 0;

        $('#btnAgregar').click(function (e)
        {
            e.preventDefault();

            let id_producto = $('#producto_id').val();
            let cantidad_producto = Number.parseFloat($('#cantidad_producto').val());
            //let detalle = [];

            if(id_producto == '' || id_producto == undefined ||  id_producto == 0 || cantidad_producto == 0 || cantidad_producto == '' ){

                Swal.fire(
                    'Advertencia!',
                    'Seleccione producto y cantidad para agregar',
                    'warning'
                  )

            }else{

               
                let nombre_producto = $('#producto_id :selected').text();
                let precio_producto = Number.parseFloat($('#precio_producto').val());
                let subtotal_venta = (cantidad_producto  * precio_producto).toFixed(2);
                console.log('subtotal ', subtotal_venta );
                let total_input = ($('#total').val() * 100 / 100);
                console.log('total_input ' + total_input.toFixed(2));
                let total =   (total_input * 100 /100 )  + (subtotal_venta * 100 /100);
                console.log(total.toFixed(2));
                $('#total').val(total.toFixed(2));
    
                var $fila = $('<tr/>', {'data-subtotal': subtotal_venta});
                var $celdaA = $('<td />');
                var $celdaB = $('<td />');
                var $celdaC = $('<td />');
                var $celdaD = $('<td />');
                var $celdaE = $('<td />');
                var $celdaF = $('<td />');
    
                //Nombres
                var indices = 'indice[' + indice + ']';
                var ids = 'id[' + indice + ']';
                var productos = 'producto[' + indice + ']';
                var precios = 'precio[' + indice + ']';
                var cantidades = 'cantidad[' + indice + ']';
                var subtotales = 'subtotal[' + indice + ']';
    
    
                //Elemento de columna
                var $id = $('<input />', { type: 'text', name: ids, class: 'form-control', value: id_producto, 'readonly': 'readonly' });
                var $producto = $('<input />', { type: 'text', name: productos, class: 'form-control', value: nombre_producto, 'readonly': 'readonly' });
                var $precio = $('<input />', { type: 'text', name: precios, class: 'form-control', value: precio_producto, 'readonly': 'readonly' });
                var $cantidad = $('<input />', { type: 'text', name: cantidades, class: 'form-control', value: cantidad_producto, 'readonly': 'readonly' });
                var $subtotal = $('<input />', { type: 'text', name: subtotales, class: 'form-control', value: subtotal_venta, 'readonly': 'readonly' });
                var $delete = $('<button />', { type: 'button', class: 'btn btn-danger'}).text('Eliminar');

                var $indices = $('<input />', { type: 'text', name: indices, class: 'form-control', value: indice, 'hidden': 'hidden' });
    
                
                //Agregando dato a la columna
                $celdaA.append($id);
                $celdaB.append($producto);
                $celdaC.append($precio);
                $celdaD.append($cantidad);
                $celdaE.append($subtotal);
                $celdaF.append($delete);
                $celdaF.append($indices);
    
                //Contruyendo la fila
                $fila.append([$celdaA, $celdaB, $celdaC, $celdaD, $celdaE, $celdaF, $celdaF]);
    
                $('#tbl-productos').append($fila);

                $delete.on('click', (e2)=> {
                    e2.preventDefault();
                   

                    let total_actual = ($('#total').val() * 100 /100 );
                    let subtotal_fila = ($fila.data('subtotal') * 100 /100);
                    total_actual = total_actual.toFixed(2);
                    subtotal_fila = subtotal_fila.toFixed(2);
                    let diferencia = total_actual - subtotal_fila;
                    $('#total').val(diferencia.toFixed(2));
                    $fila.remove();
                    //console.log("Delete");
                })    
    
                indice++;
    
                cantidad_producto = 0;
                nombre_producto = '';
                precio_producto = '';
                id_producto = '';
                subtotal_venta = '';
    
                $('#cantidad_producto').val(0);
                $('#precio_producto').val('');
                $('#producto_id').val('');
                $('#producto_id').change();

            }
            
        });
    });

    var validator = $("#VentaForm").validate({
        ignore: [],
        onkeyup:false,
        onclick: false,
        //onfocusout: false,
        rules: {
            cliente:{
                required: true,
            },
    
        },
        messages: {
            cliente: {
                required: "Por favor, seleccione cliente"
            },
        }
    });

    $('#producto_id').on('change', (e) =>{
        var producto_id = $('#producto_id').val();

        axios.get(APP_URL+`/productos/buscarPrecio/${producto_id}`)
            .then(function (response) {
                $('#precio_producto').val(response.data.precio);
                $('#cantidad_producto').val('');
            })
            .catch(function (error) {
                alert("Ocurrio un error!");
            })
    });