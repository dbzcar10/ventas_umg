var validator = $("#ClienteForm").validate({
    ignore: [],
    onkeyup: false,
    onclick: false,
    //onfocusout: false,
    rules: {
        nombre: {
            required: true
        },
        nit: {
            required: true,
        },
    },
    messages: {
        nombre: {
            required: "Por favor, ingrese nombre"
        },
        nit: {
            required: "Por favor, ingrese nit",
        },
    }
});
