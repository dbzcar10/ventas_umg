<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{asset('css/app.css') }}">

  <title>{{config('app.name')}}</title>

 
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.min.css')}}">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{asset('admintemplate/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

    <!-- Styles pare datatables-->
    <link rel="stylesheet" href="{{asset('DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{asset('DataTables/Buttons-1.5.4/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{asset('DataTables/Responsive-2.2.2/css/responsive.dataTables.min.css') }}">

    <link rel="stylesheet" href="{{asset('css/alertify.css') }}">

    <link rel="stylesheet" href="{{asset('css-loader-master/dist/css-loader.css') }}">

    <link href="{{asset('admintemplate/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />


  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admintemplate/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('fonts/Source+Sans+Pro/Source+Sans+Pro.css')}}" rel="stylesheet">

  <link href="{{asset('css/style.css')}}" rel="stylesheet">


  @stack('styles')
</head>

<body class="hold-transition sidebar-mini">
    {{--<div class="loader loader-bar is-active"></div>--}}


  <div class="wrapper" id="app">


    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-dark navbar-info">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fa fa-user"></i> {{@Auth::user()->name}}
            <!--<span class="badge badge-warning navbar-badge">15</span>-->
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            {{--<span class="dropdown-header">{{ auth()->user()->name }}</span>
            <div class="dropdown-divider"></div>--}}

            <div class="dropdown-divider"></div>
            {{--<a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>--}}
            <form  method="POST" action=" {{ route('logout') }} ">
              {{ csrf_field() }}
              <button class="dropdown-item"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Cerrar Sesión</button>
            </form>
          </div>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      @include('admin.partials.nav')
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      @include('admin.partials.alerts')

      @yield('header')
      <!-- /.content-header -->

      <!-- Main content -->
      @yield('contenido')
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    {{--<aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>--}}
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        {{config('app.name')}} - Curso Elaboración de Paginas Web
      </div>
      <!-- Default to the left -->
      <strong>Copyright &copy; {{date('Y')}} <a href="#">Carlos Benjamín Morales Orellana</a>.</strong> Todos los derechos reservados
    </footer>
  </div>
  <!-- ./wrapper -->



  <script type="text/javascript">
      var APP_URL = {!! json_encode(url('/')) !!}
  </script>

  <!-- REQUIRED SCRIPTS -->
   <!-- jQuery -->
  <script src="{{asset('js/app.js') }}"></script>

  <!-- SweetAlert2 -->
  <script src="{{asset('admintemplate/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

  <!-- DataTables -->
  <script src="{{asset('DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{asset('DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{asset('DataTables/Buttons-1.5.4/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{asset('DataTables/Responsive-2.2.2/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{asset('DataTables/Buttons-1.5.4/js/buttons.html5.min.js') }}"></script>
  <script src="{{asset('DataTables/JSZip-2.5.0/jszip.min.js') }}"></script>
  <script src="{{asset('DataTables/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
  <script src="{{asset('DataTables/pdfmake-0.1.36/vfs_fonts.js') }}"></script>

  <!-- moment -->
  <script src="{{asset('js/moment.min.js')}}"></script>
  <script src="{{asset('js/datetime-moment.js')}}"></script>

  <script src="{{asset('js/jquery.validate.js') }}"></script>
  <script src="{{asset('js/alertify.js') }}"></script>
  <script src="{{asset('admintemplate/plugins/select2/js/select2.full.min.js')}}"></script>

  <!-- ChartJS -->
  <script src="{{asset('admintemplate/plugins/chart.js/Chart.min.js')}}"></script>

  <!-- AdminLTE App -->
  <script src="{{asset('admintemplate/js/adminlte.min.js')}}"></script>



  <script>

      //Inicializa las alertas
      alertify.defaults = {
            // dialogs defaults
            autoReset:true,
            basic:false,
            closable:true,
            closableByDimmer:true,
            frameless:false,
            maintainFocus:true, // <== global default not per instance, applies to all dialogs
            maximizable:true,
            modal:true,
            movable:true,
            moveBounded:false,
            overflow:true,
            padding: true,
            pinnable:true,
            pinned:true,
            preventBodyShift:false, // <== global default not per instance, applies to all dialogs
            resizable:true,
            startMaximized:false,
            transition:'pulse',

            // notifier defaults
            notifier:{
                // auto-dismiss wait time (in seconds)
                delay:5,
                // default position
                position:'bottom-right',
                // adds a close button to notifier messages
                closeButton: false
            },

            // language resources
            glossary:{
                // dialogs default title
                title:'Aviso!',
                // ok button text
                ok: 'OK',
                // cancel button text
                cancel: 'Cancelar'
            },

            // theme settings
            theme:{
                // class name attached to prompt dialog input textbox.
                input:'ajs-input',
                // class name attached to ok button
                ok:'ajs-ok',
                // class name attached to cancel button
                cancel:'ajs-cancel'
            }
      };

      //Inicializa Las tablas

        //$.fn.dataTable.render.moment( 'DD/MM/YYYY HH:mm' );
        $.extend( $.fn.dataTable.defaults, {

          "serverSide": true,
          "processing": true,
          "showNEntries": true,

          lengthMenu: [
              [ 10, 25, 50, -1 ],
              [ '10 filas', '25 filas', '50 filas', 'Mostrar todo' ]
          ],

          "responsive" : true,
          "info": true,
          "dom": 'Bfrtip',

          "buttons": [
          'pageLength',
          {
              extend: 'pdfHtml5',
          },
          'excelHtml5',
          'csvHtml5',

          ],

          "paging": true,


          "language": {
              "sdecimal":        ".",
              "sthousands":      ",",
              "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "zeroRecords": "No hay coincidencias",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              },
          }
      } );
  </script>

@stack('scripts')
</body>

</html>
