
@if(session()->has('success'))
<div class="alert alert-success">{{ session('success') }}
    <a href="#" class="close" data-dismiss="alert">&times;</a>
</div>
@elseif(session()->has('alerta'))
<div class="alert alert-warning">{{ session('alerta') }}
    <a href="#" class="close" data-dismiss="alert">&times;</a>
</div>
@elseif(session()->has('danger'))
<div class="alert alert-danger">{{ session('danger') }}
    <a href="#" class="close" data-dismiss="alert">&times;</a>
</div>
@elseif(session()->has('errors2'))
<div class="alert alert-danger">{{session('errors2')}}
    <a href="#" class="close" data-dismiss="alert">&times;</a>
</div>
@endif


@push('scripts')
<script>
    alertify.set('notifier', 'position', 'top-center');

    @if(session()->has('successToastr'))
        alertify.success("{{session('successToastr')}}");
    @elseif(session()->has('alertaToastr'))
        alertify.success("{{session('alertaToastr')}}");
    @elseif(session()->has('dangerToastr'))
        alertify.success("{{session('dangerToastr')}}");
    @elseif(session()->has('errors2Toastr'))
        alertify.success("{{session('errors2Toastr')}}");
    @endif
</script>
@endpush