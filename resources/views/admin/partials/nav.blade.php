<!-- Brand Logo -->
<a href="#" class="brand-link">
    <img src="{{asset('admintemplate/img/AdminLTELogo.png')}}" alt="{{config('app.name')}}" class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">{{config('app.name')}}</span>
</a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column text-sm" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->

      @if(auth()->check())
        <li class="nav-item">
          <a href="{{route('dashboard')}}" class="nav-link {{setActiveRoute('dashboard')}}">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>Dashboard</p>
          </a>
        </li>
      @endif
      
      <li class="nav-item">
        <a href="{{route('clientes')}}" class="nav-link {{setActiveRoute('clientes')}}">
          <i class="nav-icon fas fa-id-card"></i>
          <p>Clientes</p>
        </a>
      </li>

      <li class="nav-item">
        <a href="{{route('productos')}}" class="nav-link {{setActiveRoute('productos')}}">
          <i class="nav-icon fas fa-book-open"></i>
          <p>Productos</p>
        </a>
      </li>

      <li class="nav-item">
        <a href="{{route('ventas')}}" class="nav-link {{setActiveRoute('ventas')}}">
          <i class="nav-icon fas fa-money-bill-wave"></i>
          <p>Ventas</p>
        </a>
      </li>
        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
