@extends('admin.layoutadmin')

@section('header')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Editar Cliente</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('clientes')}}">Clientes</a></li>
            <li class="breadcrumb-item active">Editar Cliente</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('contenido')
<div class="content">
    <div class="container-fluid">
      <div class="card card-outline">
        <div class="card-body">
            <form method="POST" action="{{route('clientes.update', $cliente)}}" id="ClienteForm">
                {{csrf_field()}} {{ method_field('PUT') }}

                <div class="form-row"><!--form-row-->
                  <div class="form-group col-sm-4">
                      <label for="nombre">Nombre:</label>
                      <input type="text" name="nombre" placeholder="Ingrese Nombre" class="form-control" value="{{old('nombre', $cliente->nombre)}}">
                      {!! $errors->first('nombre', '<div class="error">:message</div>') !!}
                  </div>
  
                  <div class="form-group col-sm-4">
                    <label for="nit">Nit:</label>
                    <input type="text" name="nit" placeholder="Ingrese Nit" class="form-control" value="{{old('nit', $cliente->nit)}}">
                    {!! $errors->first('nit', '<div class="error">:message</div>') !!}
                  </div>

                  <div class="form-group col-sm-4">
                    <label for="celular">Celular:</label>
                    <input type="text" name="celular" placeholder="Ingrese Teléfono" class="form-control" value="{{old('celular', $cliente->celular)}}">
                  </div>
                  
                </div>  

                <div class="text-right m-t-15">
                    <a href="{{route('clientes')}}" class="btn btn-default">Regresar</a>
                    <button class="btn btn-primary" id="btnActualizar">Actualizar</button>
                </div>

                <div class="loader loader-bar is-active"></div>
            </form>

        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection


@push('styles')
    <style>
    </style>
@endpush

@push('scripts')
<script src="{{asset('js/jquery.mask.js')}}"></script>

<script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
    });
    $("#btnActualizar").click(function(event) {
        event.preventDefault();
        if ($('#ClienteForm').valid()) {
            this.form.submit();
            this.disabled= true;
            $('#btnActualizar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Cargando...');
            //$('.loader').fadeIn(225);
        } else {
            validator.focusInvalid();
        }
    });

</script>

<script src="{{asset('js/clientes/create.js')}}"></script>

@endpush
