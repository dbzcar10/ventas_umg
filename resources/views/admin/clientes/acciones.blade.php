<div class='text-center row'>
    <div class="col-sm">
        <a href='{{route('clientes.edit', $id)}}'>
            <i class='fa fa-btn fa-edit' title='Editar Cliente' data-toggle="tooltip" data-placement="top"></i>
        </a>
    </div>

    <div class="col-sm">
        <a href='{{route('clientes.destroy', $id)}}' class='delete-cliente' data-method='delete'>
            <i class='fa fa-trash' title='Eliminar Cliente' data-toggle="tooltip" data-placement="top"></i>
        </a>
    </div>

</div>


