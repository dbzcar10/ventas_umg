@extends('admin.layoutadmin')

@section('header')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Listado de Clientes</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Clientes</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('contenido')

<div class="content">
    <div class="container-fluid">
      <div class="card card-outline">
          <div class="card-header text-right">
                <a class="btn btn-primary" href="{{route('clientes.create')}}">
                <i class="fa fa-plus"></i>Agregar Cliente</a>
          </div>
        <div class="card-body">
            <table class="table table-striped table-bordered no-margin-bottom dt-responsive nowrap" id="clientes-table" width="100%">
                <thead>
                  <tr>
                    <th>Nit</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
            </table>
        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<div class="loader loader-bar is-active"></div>
@endsection


@push('styles')

@endpush

@push('scripts')

<script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
      $('table').on('draw.dt', function() {
          $('[data-toggle="tooltip"]').tooltip();
      })

    });

      let clientes_table = $('#clientes-table').DataTable({
            "ajax": "{{route('clientes.getJson')}}",
            "columns": [
              { "data": "nit" },
              { "data": "nombre" },
              { "data": "btn", orderable: false, searchable: false },
            ],
        });


        $(document).on('click', 'a.delete-cliente', function(e) {
          e.preventDefault();
          alertify.set('notifier', 'position', 'top-center');

          var $this = $(this);
          alertify.confirm('Eliminar Cliente', 'Esta seguro de eliminar el cliente',
          function() {
              axios.delete($this.attr('href') )
              .then(function (response) {
                clientes_table.ajax.reload(null, false);
                  
                alertify.success('Cliente Eliminado con Éxito!!');
              })
              .catch(function (error) {
                if(error.response){

                  if (error.response.status == 403) {
                  alertify.error('Usuario no autorizado para realizar esta accíon');
                  } else {
                      alertify.error('Ocurrio un error contacte al administrador!');
                  }

                }else{
                  console.log(error.message);
                  alertify.error('Ocurrio un error contacte al administrador!');
                }
                              
              });
          },
          function() {
              alertify.error('Cancelar')
          });
      });

</script>

@endpush
