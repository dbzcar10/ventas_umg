@extends('admin.layoutadmin')

@section('header')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Listado de Ventas</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Ventas</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('contenido')

<div class="content">
    <div class="container-fluid">
      <div class="card card-outline">
          <div class="card-header text-right">
                <a class="btn btn-primary" href="{{route('ventas.create')}}">
                <i class="fa fa-plus"></i>Agregar Venta</a>
          </div>
        <div class="card-body">
            <table class="table table-striped table-bordered no-margin-bottom dt-responsive nowrap" id="ventas-table" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Fecha</th>
                    <th>Total</th>
                    <th>Nombre Cliente</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
            </table>
        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<div class="loader loader-bar is-active"></div>
@endsection


@push('styles')

@endpush

@push('scripts')

<script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
      $('table').on('draw.dt', function() {
          $('[data-toggle="tooltip"]').tooltip();
      })

    });

      let ventas_table = $('#ventas-table').DataTable({
            "ajax": "{{route('ventas.getJson')}}",
            "columns": [
              { "data": "id" },
              { "data": "created_at" },
              { "data": "total" },
              { "data": "cliente.nombre" },
              { "data": "btn", orderable: false, searchable: false },
            ],
        });

</script>

@endpush
