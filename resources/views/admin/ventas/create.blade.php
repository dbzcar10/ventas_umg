@extends('admin.layoutadmin')

@section('header')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Crear Venta</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('ventas')}}">ventas</a></li>
            <li class="breadcrumb-item active">Crear Venta</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('contenido')
<div class="content">
    <div class="container-fluid">
      <div class="card card-outline">
        <div class="card-body">
            <form method="POST" action="{{route('ventas.store')}}" id="VentaForm">
              {{csrf_field()}}
              <div class="form-row"><!--form-row-->

                <div class="form-group col-sm-4">
                    <label for="nombre">Cliente:</label>
                    <select name="cliente" id="cliente" class="form-control">
                      <option value="">Seleccione Cliente</option>
                      @foreach ($clientes as $cliente )
                        <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
                      @endforeach
                    </select>
                    {!! $errors->first('cliente', '<div class="error">:message</div>') !!}
                </div>
                
              </div>

              {{-- Productos--}}
              <div class="card shadow mb-4">
                  <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Detalle de ventas</h6>
                  </div>
                  <div class="card-body">
                    <div class="form-row">
                      <div class="form-group col-sm-3">
                        <label for="producto">Producto</label>
                        <select name="producto_id" id="producto_id" class="form-control">
                          <option value="" selected disabled>Seleccione Producto</option>
                          @foreach ($productos as $producto )
                            <option value="{{$producto->id}}">{{$producto->nombre}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group col-sm-3">
                        <label for="precio_producto">Precio</label>
                        <input type="number" name="precio_producto" class="form-control" readonly id="precio_producto" value="0.0">
                      </div>
                      <div class="form-group col-sm-3">
                        <label for="cantidad_producto">Cantidad</label>
                        <input type="number" name="cantidad_producto" class="form-control" id="cantidad_producto" value="0">
                      </div>
                      <div class="form-group col-sm-3 text-center mt-4">
                          <button class="btn btn-success" id="btnAgregar" type="button">Agregar</button>
                      </div>
                    </div>


                      <br>
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered no-margin-bottom dt-responsive nowrap" width="100%">
                              <thead class="thead-dark">
                                  <tr>
                                      <th>ID Producto</th>
                                      <th>Producto</th>
                                      <th>Precio</th>
                                      <th>Cantidad</th>
                                      <th>Subtotal</th>
                                      <th>Acciones</th>
                                  </tr>
                              </thead>
                              <tbody id="tbl-productos"></tbody>
                          </table>
                      </div>
                      <div class="form-row">
                        <div class="text-right col-sm-2 offset-10">
                          <label for="total">Total</label>
                          <input type="number"  name="total" id="total" value='0.00' class="form-control" style="font-size: 1.5rem" readonly>
                        </div>
                      </div>
                      
                  </div>
              </div>


                <div class="text-right m-t-15">
                    <a href="{{route('ventas')}}" class="btn btn-default">Regresar</a>
                    <button class="btn btn-primary" id="btnGuardar">Guardar</button>
                </div>

                <div class="loader loader-bar is-active"></div>
            </form>

        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection


@push('styles')

@endpush

@push('scripts')
<script src="{{asset('js/jquery.mask.js')}}"></script>

<script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
      $('#cliente').select2();
      $('#producto_id').select2();
    });
    $("#btnGuardar").click(function(event) {
        event.preventDefault();
        if ($('#VentaForm').valid()) {
            this.form.submit();
            this.disabled= true;
            $('#btnGuardar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Cargando...');
            //$('.loader').fadeIn(225);
        } else {
            validator.focusInvalid();
        }
    });
</script>
<script src="{{asset('js/ventas/create.js')}}"></script>
@endpush
