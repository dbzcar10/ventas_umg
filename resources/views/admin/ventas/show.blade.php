@extends('admin.layoutadmin')

@section('header')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Detalle Venta</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('ventas')}}">ventas</a></li>
            <li class="breadcrumb-item active">Detalle Venta</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('contenido')
<div class="content">
    <div class="container-fluid">
      <div class="card card-outline">
        <div class="card-body">
              <div class="form-row"><!--form-row-->

                <div class="form-group col-sm-4">
                    <label for="nombre">No.Venta:</label>
                    <p>{{$venta->id}}</p>
                </div>

                <div class="form-group col-sm-4">
                  <label for="nombre">Cliente:</label>
                  <p>{{$venta->cliente->nombre}}</p>
                </div>

                <div class="form-group col-sm-4">
                  <label for="nombre">Fecha y Hora:</label>
                  <p>{{$venta->created_at->format('d-m-y H:m') }}</p>
                </div>
                
              </div>

              {{-- Productos--}}
              <div class="card shadow mb-4">
                  <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Detalle</h6>
                  </div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table class="table table-striped table-bordered no-margin-bottom dt-responsive nowrap" width="100%">
                              <thead class="thead-dark">
                                  <tr>
                                      <th>ID Producto</th>
                                      <th>Producto</th>
                                      <th>Precio</th>
                                      <th>Cantidad</th>
                                      <th>Subtotal</th>
                                  </tr>
                              </thead>
                              <tbody id="tbl-productos">
                                @foreach ( $venta->detalles as $detalle )
                                  <tr>
                                    <td>{{$detalle->producto_id}}</td>
                                    <td>{{$detalle->producto->nombre}}</td>
                                    <td>Q {{number_format($detalle->precio, 2, '.', ',')}}</td>
                                    <td>{{$detalle->cantidad}}</td>
                                    <td>Q {{number_format($detalle->total, 2, '.', ',')}}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                          </table>
                      </div>
                      <div class="form-row">
                        <div class="text-right col-sm-2 offset-10">
                          <label for="total">Total</label>
                          <input type="text"  name="total" value='Q {{number_format($venta->total, 2, '.', ',')}}' class="form-control" style="font-size: 1.5rem" readonly>
                        </div>
                      </div>
                      
                  </div>
              </div>


                <div class="text-right m-t-15">
                    <a href="{{route('ventas')}}" class="btn btn-default">Regresar</a>
                </div>

                <div class="loader loader-bar is-active"></div>

        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection


@push('styles')

@endpush

@push('scripts')
<script src="{{asset('js/jquery.mask.js')}}"></script>

<script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
    });
</script>
@endpush
