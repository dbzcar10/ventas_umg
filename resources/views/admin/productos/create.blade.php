@extends('admin.layoutadmin')

@section('header')
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Crear Producto</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('productos')}}">productos</a></li>
            <li class="breadcrumb-item active">Crear Producto</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('contenido')
<div class="content">
    <div class="container-fluid">
      <div class="card card-outline">
        <div class="card-body">
            <form method="POST" action="{{route('productos.store')}}" id="ProductoForm">
              {{csrf_field()}}
              <div class="form-row"><!--form-row-->
                <div class="form-group col-sm-6">
                    <label for="nombre">Nombre:</label>
                    <input type="text" name="nombre" placeholder="Ingrese Nombre" class="form-control" value="{{old('nombre')}}">
                    {!! $errors->first('nombre', '<div class="error">:message</div>') !!}
                </div>

                <div class="form-group col-sm-6">
                  <label for="precio">precio:</label>
                  <input type="text" name="precio" placeholder="Ingrese Precio" class="form-control" value="{{old('precio')}}">
                  {!! $errors->first('precio', '<div class="error">:message</div>') !!}
                </div>
                
              </div>

                <div class="text-right m-t-15">
                    <a href="{{route('productos')}}" class="btn btn-default">Regresar</a>
                    <button class="btn btn-primary" id="btnGuardar">Guardar</button>
                </div>

                <div class="loader loader-bar is-active"></div>
            </form>

        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection


@push('styles')

@endpush

@push('scripts')
<script src="{{asset('js/jquery.mask.js')}}"></script>

<script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
    });
    $("#btnGuardar").click(function(event) {
        event.preventDefault();
        if ($('#ProductoForm').valid()) {
            this.form.submit();
            this.disabled= true;
            $('#btnGuardar').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Cargando...');
            //$('.loader').fadeIn(225);
        } else {
            validator.focusInvalid();
        }
    });
</script>
<script src="{{asset('js/productos/create.js')}}"></script>
@endpush
