<div class='text-center row'>
    <div class="col-sm">
        <a href='{{route('productos.edit', $id)}}'>
            <i class='fa fa-btn fa-edit' title='Editar Producto' data-toggle="tooltip" data-placement="top"></i>
        </a>
    </div>

    <div class="col-sm">
        <a href='{{route('productos.destroy', $id)}}' class='delete-producto' data-method='delete'>
            <i class='fa fa-trash' title='Eliminar Producto' data-toggle="tooltip" data-placement="top"></i>
        </a>
    </div>

</div>


