@extends('admin.layoutadmin')

@section('header')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Listado de Productos</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li class="breadcrumb-item active">Productos</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('contenido')

<div class="content">
    <div class="container-fluid">
      <div class="card card-outline">
          <div class="card-header text-right">
                <a class="btn btn-primary" href="{{route('productos.create')}}">
                <i class="fa fa-plus"></i>Agregar Producto</a>
          </div>
        <div class="card-body">
            <table class="table table-striped table-bordered no-margin-bottom dt-responsive nowrap" id="productos-table" width="100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>precio</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
            </table>
        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<div class="loader loader-bar is-active"></div>
@endsection


@push('styles')

@endpush

@push('scripts')

<script>
    $(document).ready(function() {
      $('.loader').fadeOut(225);
      $('table').on('draw.dt', function() {
          $('[data-toggle="tooltip"]').tooltip();
      })

    });

      let productos_table = $('#productos-table').DataTable({
            "ajax": "{{route('productos.getJson')}}",
            "columns": [
              { "data": "id" },
              { "data": "nombre" },
              { "data": "precio",
                "render": $.fn.dataTable.render.number(',', '.', 2, 'Q')
              },
              { "data": "btn", orderable: false, searchable: false },
            ],
        });

        $(document).on('click', 'a.delete-producto', function(e) {
          e.preventDefault();
          alertify.set('notifier', 'position', 'top-center');

          var $this = $(this);
          alertify.confirm('Eliminar Producto', 'Esta seguro de eliminar el producto',
          function() {
              axios.delete($this.attr('href') )
              .then(function (response) {
                productos_table.ajax.reload(null, false);
                  
                alertify.success('Producto Eliminado con Éxito!!');
              })
              .catch(function (error) {
                if(error.response){

                  if (error.response.status == 403) {
                  alertify.error('Usuario no autorizado para realizar esta accíon');
                  } else {
                      alertify.error('Ocurrio un error contacte al administrador!');
                  }

                }else{
                  console.log(error.message);
                  alertify.error('Ocurrio un error contacte al administrador!');
                }
                              
              });
          },
          function() {
              alertify.error('Cancelar')
          });
      });
</script>

@endpush
