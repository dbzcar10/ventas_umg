@extends('layouts.app')

@section('content')
<div class="container pt-2">
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
            <img src="{{asset('img/umg.png')}}" class="img-fluid">
            
            <h2 class="text-center pt-3">Sistema de Ventas UMG</h2>

            <img src="{{asset('img/ventas.jpg')}}" alt="" class="img-fluid">
        </div>
    </div>
</div>
@endsection