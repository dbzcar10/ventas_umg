<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Producto;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //if(!$request->ajax()) return abort('403');

        $productos = Producto::all();

        return response()->json(['data' => $productos], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::where('id', $id)->first();
        if($producto){
            return response()->json(['data' => $producto], 200);
        }else{
            return response()->json(['Error' => 'El Producto no existe'], 404);
        }
        
    }

}
