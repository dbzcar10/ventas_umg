<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Venta;
use App\Models\Cliente;
use App\Models\Producto;
use App\Models\VentaDetalle;
use Illuminate\Support\Facades\Response;
use Exception;
use DB;
use Illuminate\Support\Collection;

class VentasController extends Controller
{
    public function index()
    {
        return view('admin.ventas.index');
    }

    public function getJson(Request $request)
    {
        if(!$request->ajax()) return abort('403');

        //$ventas = Venta::all();

        return datatables()
        ->eloquent(Venta::with('cliente'))
        ->editColumn('created_at', function($request){
            return $request->created_at->format('d-m-y H:m');
        })
        ->addColumn('btn', 'admin.ventas.acciones')
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function create()
    {
        $clientes = Cliente::all();
        $productos = Producto::all();
        return view('admin.ventas.create', compact('clientes', 'productos'));
    }

    public function store(Request $request)
    {
       $request->validate($this->reglas());

       //dd($request->all());

        try{
            DB::beginTransaction();

            if($request->has('indice') )
            {

                //$size = count(collect($request->id));

                $venta = new Venta();
                $venta->cliente_id = $request->cliente;
                $venta->user_id = auth()->user()->id;
                $venta->total = $request->total;
                $venta->save();

                foreach ($request->indice as $key => $indice) {
                    $detalle = new VentaDetalle;
                    $detalle->precio = $request->precio[$indice];
                    $detalle->cantidad = $request->cantidad[$indice];
                    $detalle->total = $request->subtotal[$indice];
                    $detalle->producto_id = $request->id[$indice];
                    $detalle->venta_id = $venta->id;
                    $detalle->save();
                }
                
            }else {
                return redirect()->back()->withAlerta('Es necesario agregar un producto a la venta');
            }

            DB::commit();

            return redirect()->route('ventas')->withSuccess('La venta se registro correctamente.');


        }catch(Exception $e){
            DB::rollback();
            //return redirect()->route('ventas')->withDanger('Ocurrio un error al registrar la venta.');
            dd($e);
        }

        

    }

    public function show (Venta $venta)
    {
        return view('admin.ventas.show', compact('venta'));
    }


    protected function reglas()
    {
        return  [
            'cliente' => 'required',
        ];
    }
}
