<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Validation\Rule;
use App\Rules\NitUnico;

class ClientesController extends Controller
{
    public function index()
    {
        return view('admin.clientes.index');
    }

    public function getJson(Request $request)
    {
        if(!$request->ajax()) return abort('403');

        //$clientes = Cliente::all();

        return datatables()
        ->eloquent(Cliente::query())
        ->addColumn('btn', 'admin.clientes.acciones')
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function create()
    {
        return view('admin.clientes.create');
    }

    public function store(Request $request)
    {
        $clientes = Cliente::all();
        $campos = $request->validate($this->reglas($clientes));

        $cliente = Cliente::create($campos);

        return redirect()->route('clientes')->withSuccess('El cliente ha sido creado');

    }


    public function edit(Cliente $cliente)
    {

        return view('admin.clientes.edit', compact('cliente'));
    }

    public function update(Request $request, Cliente $cliente)
    {
        $clientes = Cliente::where('id', '!=', $cliente->id)->get();

        $reglas = [
            'nombre' => 'required',
            'nit' => ['required', new NitUnico($clientes)],
            'celular' => ''
        ];

        $campos = $request->validate($reglas);
    
        $cliente->update($campos);

        return redirect()->route('clientes')->withSuccess('El cliente ha sido actualizado');
    }

    public function destroy(Cliente $cliente)
    {
        $cliente->delete();
        return response()->json(['data' => 'Éxito'], 200);
    }

    protected function reglas($coleccion)
    {
        return  [
            'nombre' => 'required',
            'nit' => ['required',  new NitUnico($coleccion) ],
            'celular' => ''
        ];
    }
}
