<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use Illuminate\Support\Facades\Response;

class ProductosController extends Controller
{
    public function index()
    {
        return view('admin.productos.index');
    }

    public function getJson(Request $request)
    {
        if(!$request->ajax()) return abort('403');

        //$productos = Producto::all();

        return datatables()
        ->eloquent(Producto::query())
        ->addColumn('btn', 'admin.productos.acciones')
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function create()
    {
        return view('admin.productos.create');
    }

    public function store(Request $request)
    {
        $campos = $request->validate($this->reglas());

        $producto = Producto::create($campos);

        return redirect()->route('productos')->withSuccess('El producto ha sido creado');

    }


    public function edit(Producto $producto)
    {

        return view('admin.productos.edit', compact('producto'));
    }

    public function update(Request $request, Producto $producto)
    {

        $campos = $request->validate($this->reglas());
    
        $producto->update($campos);

        return redirect()->route('productos')->withSuccess('El producto ha sido actualizado');
    }

    protected function reglas()
    {
        return  [
            'nombre' => 'required',
            'precio' => 'required',
        ];
    }

    public function buscarPrecio($id)
    {
        $producto = Producto::where('id', $id)->first();
        if($producto){
            return response::json(['precio' => $producto->precio]);
        }else{
            return response::json(['precio' => 0]);
        }
        
    }

    public function destroy(Producto $producto)
    {
        $producto->delete();
        return response()->json(['data' => 'Éxito'], 200);
    }
}
